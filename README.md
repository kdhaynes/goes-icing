Detecting Icing Conditions from the Geostationary Operational Environmental Satellite (GOES-16)

<br>
1. Model Search: Icing_MSearch.ipynb

   - Random and guided search using Hyperopt
   - Selects optimal pre-processing technique
     - Image flattening
     - Attribute/feature selection
     - Normalization
     - Scaling
   * Determines optimal classification model
      * AdaBoost
      * Decision Tree
      * Gradient Boosting
      * k-Nearest Neighbors
      * Random Forest
      * Support Vector Classification
<br>
2. Multilayer Perceptron Model (Neural Network)

   * Architecture and hyperparameter optimization using Hyperopt  
<br>
3. Convolutional Neural Network (CNN)

   * Architecture and hyperparameter optimization using Hyperopt

<br>
NOTE: Data is NOT included.